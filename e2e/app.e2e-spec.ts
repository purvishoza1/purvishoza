import { ProAngularPage } from './app.po';

describe('pro-angular App', function() {
  let page: ProAngularPage;

  beforeEach(() => {
    page = new ProAngularPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
